from django.urls import path, include
from . import views
from rest_framework import routers#responsble for generating all urls for the models

router = routers.DefaultRouter()
router.register('movies', views.LanguageView)

urlpatterns = [
    path('', include(router.urls)),
]