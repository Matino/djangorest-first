from django.shortcuts import render
from rest_framework import viewsets
from .models import Movies
from .serializers import MoviesSerializer

class LanguageView(viewsets.ModelViewSet):
    queryset = Movies.objects.all()#to return all objects in the Movies model
    serializer_class = MoviesSerializer
